
import 'package:flutter/material.dart';
import 'package:test/screens/select_image_widget.dart';

void main() => runApp(App());


class App extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return MaterialApp(
      home: SelectImageWidget(),
    );
  }
}